#pragma once

#include "abstract_window.hpp"

#include <memory>


#include "control.h"

namespace Gdiplus
{
    class Image;
}

namespace WinProcChanger
{
    class MainWindow : public AbstractWindow<MainWindow>
    {
    public:
        MainWindow();

        inline virtual PCWSTR getClassName() const override
        {
            return L"MyPaint";
        }
        virtual LRESULT mainHandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam ) override;

    private:
        enum class EControls : uint8_t
        {
            eC_MainButton = 1
        };

        Control m_butControl;
        WNDPROC m_prevWndProc;
        LPCWSTR m_imagePath;
        Gdiplus::Image* m_imgBackground;

        static LRESULT CALLBACK secondWindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
        LRESULT secondHandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam );

        //Msg handlers
        BOOL mainOnCreate( HWND hwnd, LPCREATESTRUCT lpCreateStruct );
        void mainOnPaint( HWND hwnd );
        void mainOnCommand( HWND hwnd, int id, HWND hwndCtl, UINT codeNotify );
        void secondOnPaint( HWND hwnd );
        void secondOnCommand( HWND hwnd, int id, HWND hwndCtl, UINT codeNotify );
        void sharedOnDestroy( HWND hwnd );
    };
}