#pragma once

#include <windows.h>

namespace WinProcChanger
{
    class Control
    {
    public:
        bool create( LPCWSTR lpClassName,
                     LPCWSTR lpText,
                     DWORD dwStyle,
                     HWND hwndParent = NULL,
                     HMENU hMenu = NULL,
                     int x = CW_USEDEFAULT,
                     int y = CW_USEDEFAULT,
                     int width = CW_USEDEFAULT,
                     int height = CW_USEDEFAULT,
                     HINSTANCE hInstance = NULL,
                     LPVOID lpParam = NULL
        );

        void move( int x, int y );

        void resize( int width, int height );

        inline HWND getWindow() const
        {
            return m_hwnd;
        }
    protected:
        HWND m_hwnd;
    };
}
