#include "main_window.h"

#include <windowsx.h>
#include <CommCtrl.h>

#include <gdiplus.h>

WinProcChanger::MainWindow::MainWindow() :
    AbstractWindow<MainWindow>(),
    m_butControl(),
    m_prevWndProc( secondWindowProc ),
    m_imgBackground( Gdiplus::Image::FromFile(L"rc/image.jpeg" ) )
{

}

LRESULT WinProcChanger::MainWindow::mainHandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch(uMsg)
    {
        HANDLE_MSG( m_hwnd, WM_CREATE, mainOnCreate );
        HANDLE_MSG( m_hwnd, WM_PAINT, mainOnPaint );
        HANDLE_MSG( m_hwnd, WM_COMMAND, mainOnCommand );
        HANDLE_MSG( m_hwnd, WM_DESTROY, sharedOnDestroy );
    default:
        return DefWindowProc( m_hwnd, uMsg, wParam, lParam );
    }
    return 0;
}

LRESULT CALLBACK WinProcChanger::MainWindow::secondWindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    MainWindow* pThis = NULL;

    if(uMsg == WM_NCCREATE)
    {
        CREATESTRUCT* pCreate = reinterpret_cast<CREATESTRUCT*>(lParam);
        pThis = reinterpret_cast<MainWindow*>(pCreate->lpCreateParams);
        SetWindowLongPtr( hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pThis) );

        pThis->m_hwnd = hwnd;
    }
    else
    {
        pThis = reinterpret_cast<MainWindow*>(GetWindowLongPtr( hwnd, GWLP_USERDATA ));
    }
    if(pThis)
    {
        return pThis->secondHandleMessage( uMsg, wParam, lParam );
    }
    else
    {
        return DefWindowProc( hwnd, uMsg, wParam, lParam );
    }
}

LRESULT WinProcChanger::MainWindow::secondHandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch(uMsg)
    {
        HANDLE_MSG( m_hwnd, WM_PAINT, secondOnPaint );
        HANDLE_MSG( m_hwnd, WM_COMMAND, secondOnCommand );
        HANDLE_MSG( m_hwnd, WM_DESTROY, sharedOnDestroy );
    default:
        return DefWindowProc( m_hwnd, uMsg, wParam, lParam );
    }
    return 0;
}

BOOL WinProcChanger::MainWindow::mainOnCreate( HWND hwnd, LPCREATESTRUCT lpCreateStruct )
{
    RECT rectWindow = {0};
    GetClientRect( m_hwnd, &rectWindow );

    m_butControl.create( L"BUTTON",
                         L"Click",
                         WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON | BS_NOTIFY,
                         m_hwnd,
                         reinterpret_cast<HMENU>(EControls::eC_MainButton),
                         (rectWindow.right - rectWindow.left) / 2 - 75,
                         (rectWindow.bottom - rectWindow.top) / 2 - 25,
                         150,
                         50,
                         NULL,
                         0
    );

    return TRUE;
}

void WinProcChanger::MainWindow::mainOnPaint( HWND hwnd )
{
    PAINTSTRUCT ps;
    HDC hdc = BeginPaint( m_hwnd, &ps );

    Gdiplus::Graphics graphics( ps.hdc );
    Gdiplus::SolidBrush brushBackground( {50, 151, 151} );
    graphics.FillRectangle( &brushBackground, Gdiplus::Rect( ps.rcPaint.left, ps.rcPaint.top, ps.rcPaint.right, ps.rcPaint.bottom ) );

    EndPaint( m_hwnd, &ps );
}

void WinProcChanger::MainWindow::mainOnCommand( HWND hwnd, int id, HWND hwndCtl, UINT codeNotify )
{
    if(id == static_cast<int>(EControls::eC_MainButton) && codeNotify == BN_DBLCLK)
    {
        m_prevWndProc = reinterpret_cast<WNDPROC>(SetWindowLongPtr( hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&secondWindowProc) ));
        RECT rectWnd = {0};
        GetClientRect( m_hwnd, &rectWnd );
        InvalidateRect( m_hwnd, &rectWnd, FALSE );
    }
}

void WinProcChanger::MainWindow::secondOnPaint( HWND hwnd )
{
    PAINTSTRUCT ps;
    HDC hdc = BeginPaint( m_hwnd, &ps );

    Gdiplus::Graphics graphics( ps.hdc );
    graphics.DrawImage( m_imgBackground, Gdiplus::Rect( ps.rcPaint.left, ps.rcPaint.top, ps.rcPaint.right, ps.rcPaint.bottom ) );

    EndPaint( m_hwnd, &ps );
}

void WinProcChanger::MainWindow::secondOnCommand( HWND hwnd, int id, HWND hwndCtl, UINT codeNotify )
{
    if(id == static_cast<int>(EControls::eC_MainButton) && codeNotify == BN_DBLCLK)
    {
        m_prevWndProc = reinterpret_cast<WNDPROC>(SetWindowLongPtr( hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(&mainWindowProc) ));
        RECT rectWnd = {0};
        GetClientRect( m_hwnd, &rectWnd );
        InvalidateRect( m_hwnd, &rectWnd, FALSE );
    }
}

void WinProcChanger::MainWindow::sharedOnDestroy( HWND hwnd )
{
    PostQuitMessage( 0 );
}
