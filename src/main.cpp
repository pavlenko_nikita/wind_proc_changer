#include "main_window.h"

#include <gdiplus.h>


int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow )
{
    ULONG_PTR token;
    Gdiplus::GdiplusStartupInput input = {0};
    input.GdiplusVersion = 1;
    Gdiplus::GdiplusStartup( &token, &input, NULL );

    const int width = 800;
    const int height = 600;

    WinProcChanger::MainWindow window;

    if(!window.create( L"WinProcChanger",
                       WS_OVERLAPPEDWINDOW, 
                       0,
                       GetSystemMetrics(SM_CXSCREEN) / 2 - width / 2,
                       GetSystemMetrics(SM_CYSCREEN) / 2 - height / 2,
                       width,
                       height ))
    {
        return 0;
    }

    window.showWindow( nCmdShow );

    window.run();

    Gdiplus::GdiplusShutdown( token );

    return 0;
}
