#include "control.h"

bool WinProcChanger::Control::create( LPCWSTR lpClassName,
                                      LPCWSTR lpText,
                                      DWORD dwStyle,
                                      HWND hwndParent,
                                      HMENU hMenu,
                                      int x,
                                      int y,
                                      int width,
                                      int height,
                                      HINSTANCE hInstance,
                                      LPVOID lpParam )
{
    m_hwnd = CreateWindowEx( 0, lpClassName, lpText,
                             dwStyle, x, y,
                             width, height, hwndParent,
                             hMenu, hInstance, lpParam );

    return m_hwnd ? true : false;
}

void WinProcChanger::Control::move( int x, int y )
{
    RECT controlRect = {0};
    GetClientRect( m_hwnd, &controlRect );

    MoveWindow( m_hwnd,
                x,
                y,
                controlRect.right - controlRect.left,
                controlRect.bottom - controlRect.top,
                FALSE );
}

void WinProcChanger::Control::resize( int width, int height )
{
    RECT controlRect = {0};
    GetClientRect( m_hwnd, &controlRect );

    MoveWindow( m_hwnd,
                controlRect.left,
                controlRect.top,
                width,
                height,
                FALSE );
}