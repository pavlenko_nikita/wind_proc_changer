#pragma once

#include <windows.h>

namespace WinProcChanger
{
    template <class DerivedType >
    class AbstractWindow
    {
    public:
        virtual ~AbstractWindow() = default;

        static LRESULT CALLBACK mainWindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
        {
            DerivedType* pThis = NULL;

            if(uMsg == WM_NCCREATE)
            {
                CREATESTRUCT* pCreate = reinterpret_cast<CREATESTRUCT*>(lParam);
                pThis = reinterpret_cast<DerivedType*>(pCreate->lpCreateParams);
                SetWindowLongPtr( hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pThis) );

                pThis->m_hwnd = hwnd;
            }
            else
            {
                pThis = reinterpret_cast<DerivedType*>(GetWindowLongPtr( hwnd, GWLP_USERDATA ));
            }
            if(pThis)
            {
                return pThis->mainHandleMessage( uMsg, wParam, lParam );
            }
            else
            {
                return DefWindowProc( hwnd, uMsg, wParam, lParam );
            }
        };


        bool create( PCWSTR lpWindowTitle, DWORD dwStyle, DWORD dwExStyle = 0,
                     int x = CW_USEDEFAULT, int y = CW_USEDEFAULT, int width = CW_USEDEFAULT,
                     int height = CW_USEDEFAULT, HWND hwndParent = NULL, HMENU hMenu = NULL )
        {
            WNDCLASS wc = {0};

            wc.lpfnWndProc = DerivedType::mainWindowProc;
            wc.hInstance = GetModuleHandle( NULL );
            wc.lpszClassName = getClassName();

            RegisterClass( &wc );

            m_hwnd = CreateWindowEx( dwExStyle, getClassName(), lpWindowTitle,
                                     dwStyle, x, y, width, height,
                                     hwndParent, hMenu, GetModuleHandle( NULL ), this );

            return m_hwnd ? true : false;
        }

        inline void showWindow( int nCmdShow )
        {
            ShowWindow( m_hwnd, nCmdShow );
        }

        inline HWND getHandle() const
        {
            return m_hwnd;
        }

        void run()
        {
            MSG msg = {0};
            while(GetMessage( &msg, NULL, 0, 0 ))
            {
                TranslateMessage( &msg );
                DispatchMessage( &msg );
            }
        }

    protected:
        virtual PCWSTR getClassName() const = 0;
        virtual LRESULT mainHandleMessage( UINT uMsg, WPARAM wParam, LPARAM lParam ) = 0;

        HWND m_hwnd = NULL;
    };
}